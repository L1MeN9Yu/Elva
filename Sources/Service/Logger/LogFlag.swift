//
// Created by Mengyu Li on 2019/9/25.
// Copyright (c) 2019 Mengyu Li. All rights reserved.
//

public enum LogFlag {
    case trace
    case debug
    case info
    case warn
    case error
    case crit
    case off
}